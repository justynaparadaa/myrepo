package com.exesandohs;

public class ExesAndOhs {

    public boolean checkXO(String input) {
        char literaO = 'o';
        char literaODuża = 'O';
        char literaX = 'x';
        char literaXDuża = 'X';
        int ile_literO = 0;
        int ile_literX = 0;
        char znak_z_napisuO;
        char znak_z_napisuX;


        for (int i = 0; i < input.length(); i++) {
            znak_z_napisuO = input.charAt(i);
            if (znak_z_napisuO == literaO || znak_z_napisuO == literaODuża) {
                ile_literO++;
            }

            znak_z_napisuX = input.charAt(i);
            if (znak_z_napisuX == literaX || znak_z_napisuX == literaXDuża) {
                ile_literX++;
            }
        }

        if (ile_literO == ile_literX) {
            return true;
        } else
            return false;
    }

}

